package models;

import java.util.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import play.db.jpa.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Board {

	@ElementCollection
	@Column
	public List<Integer> pits = new ArrayList<Integer>();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "game_id")
	@JsonIgnore
	public Game game;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	public static final int playingPits = 6, totalPits = 2 * (playingPits + 1),
			numOfStones = 6;

	public Board() {
		for (int pitNum = 0; pitNum < totalPits; pitNum++)
			this.pits.add(pitNum, 0);
	}

	public static Board createNewBoard() {
		Board board = new Board();
		board.setUpForPlay();
		JPA.em().persist(board);
		return board;
	}

	public void setUpForPlay() {
		for (int pitNum = 0; pitNum < totalPits; pitNum++)
			if (!isLubangMenggali(pitNum))
				addStones(pitNum, numOfStones);
	}

	public int stonesInLubangMenggali(int playerNum) {
		return getStones(lubangMenggaliIndex(playerNum));
	}

	public int stonesInPit(int playerNum, int pitNum) {
		return getStones(getPitNum(playerNum, pitNum));
	}

	public boolean makeAMove(int currentPlayerNum, int chosenPitNum) {
		int pitNum = getPitNum(currentPlayerNum, chosenPitNum);
		int stones = removeStones(pitNum);
		while (stones != 0) {
			pitNum--;
			if (pitNum < 0)
				pitNum = totalPits - 1;
			if (pitNum != lubangMenggaliIndex(otherPlayerNum(currentPlayerNum))) {
				addStones(pitNum, 1);
				stones--;
			}
		}
		if (pitNum == lubangMenggaliIndex(currentPlayerNum))
			return true;

		return false;
	}

	public boolean gameOver() {
		for (int player = 0; player < 2; player++) {
			int stones = 0;
			for (int pitNum = 1; pitNum <= playingPits; pitNum++)
				stones += getStones(getPitNum(player, pitNum));
			if (stones == 0)
				return true;
		}
		return false;
	}

	public void emptyStonesIntoLubangMenggali() {
		for (int player = 0; player < 2; player++)
			for (int pitNum = 0; pitNum <= playingPits; pitNum++) {
				int stones = removeStones(getPitNum(player, pitNum));
				addStones(lubangMenggaliIndex(player), stones);
			}
	}

	private int otherPlayerNum(int playerNum) {
		if (playerNum == 0)
			return 1;
		else
			return 0;
	}

	public int getPitNum(int playerNum, int pitNum) {
		return playerNum * (playingPits + 1) + pitNum;
	}

	private int lubangMenggaliIndex(int playerNum) {
		return playerNum * (playingPits + 1);
	}

	public boolean isLubangMenggali(int pitNum) {
		return pitNum % (playingPits + 1) == 0;
	}

	public int getStones(int number) {
		return this.pits.get(number);
	}

	public void addStones(int number, int stones) {
		int value = this.pits.get(number);
		this.pits.set(number, value + stones);
	}

	public boolean isEmpty(int number) {
		return this.pits.get(number) == 0;
	}

	public int removeStones(int number) {
		int stones = this.pits.get(number);
		this.pits.set(number, 0);
		return stones;
	}

	public static Board findById(Long id) {
		return JPA.em().find(Board.class, id);
	}

}
