package models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import play.data.validation.*;
import play.db.jpa.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Move {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@Constraints.Required
	@ManyToOne(fetch = FetchType.LAZY)
	public Player player;

	@Constraints.Required
	public int pitSize;

	@Constraints.Required
	public int pitNumber;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	public Game game;

	public Move() {
	}

	public Move(Player player, int number, int size) {
		this.player = player;
		this.pitSize = size;
		this.pitNumber = number;
	}

	public static Move createNewMove(Player player, int number, int size) {
		Move move = new Move(player, number, size);
		JPA.em().persist(move);
		return move;
	}

}
