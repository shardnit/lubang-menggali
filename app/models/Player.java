package models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import play.data.validation.*;
import play.db.jpa.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Player {

	@Constraints.Required
	public String name;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	public int playerNum;

	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	public Game game;

	@OneToMany(mappedBy = "player")
	@JsonIgnore
	public List<Move> moves = new ArrayList<Move>();

	public Player() {
	}

	public Player(String name, int playerNum) {
		this.name = name;
		this.playerNum = playerNum;
	}

	public String getName() {
		if (name != null)
			return name;
		else
			return "Computer";
	}

	public int yourMove(Board board) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter  a  pit  to  pick the stones from:  ");
		System.out.flush();
		int pitNum = Integer.parseInt(br.readLine());
		return pitNum;
	}

	public static Player createNewPlayer(String name, int number) {
		Player player = new Player(name, number);
		JPA.em().persist(player);
		return player;
	}

	public static Player findById(Long id) {
		return JPA.em().find(Player.class, id);
	}

}
