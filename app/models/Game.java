package models;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import play.data.format.*;
import play.data.validation.*;
import play.db.jpa.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@Constraints.Required
	@Formats.NonEmpty
	public String name;

	@OneToMany(mappedBy = "game")
	public List<Move> moves = new ArrayList<Move>();

	@OneToOne(mappedBy = "game", fetch = FetchType.LAZY)
	public Board board;

	@OneToMany(mappedBy = "game")
	public List<Player> players = new ArrayList<Player>();

	public Long currentPlayerId;

	public enum Status {
		IN_PROGRESS, WON, TIED
	}

	public Status status;

	@Formats.DateTime(pattern = "dd-MM-yyyy")
	@Column(name = "start_date")
	public Date startDate = new Date();

	public Game() {
	}

	public Game(String name) {
		this.name = name;
		this.status = Status.IN_PROGRESS;
	}

	public static Game findById(Long id) {
		return JPA.em().find(Game.class, id);
	}

	public static Game createNewGame(String gameName) {
		Game game = new Game(gameName);
		JPA.em().persist(game);
		return game;
	}

	public void addBoard(Board board) {
		this.board = board;
		board.game = this;
	}

	public String getStartDate() {
		return new SimpleDateFormat("dd-MM-yyyy").format(this.startDate);
	}


	/**
	 * Return a page of games
	 *
	 * @param page
	 *            Page to display
	 * @param pageSize
	 *            Number of games per page
	 * @param sortBy
	 *            game property used for sorting
	 * @param order
	 *            Sort order (either or asc or desc)
	 * @param filter
	 *            Filter applied on the name column
	 */
	public static Page page(int page, int pageSize, String sortBy,
			String order, String filter) {
		if (page < 1)
			page = 1;
		Long total = (Long) JPA
				.em()
				.createQuery(
						"select count(g) from Game g where lower(g.name) like ?")
				.setParameter(1, "%" + filter.toLowerCase() + "%")
				.getSingleResult();
		@SuppressWarnings("unchecked")
		List<Game> data = JPA
				.em()
				.createQuery(
						"from Game g where lower(g.name) like ? order by g."
								+ sortBy + " " + order)
				.setParameter(1, "%" + filter.toLowerCase() + "%")
				.setFirstResult((page - 1) * pageSize).setMaxResults(pageSize)
				.getResultList();
		return new Page(data, total, page, pageSize);
	}

	/**
	 * Used to represent a games page.
	 */
	public static class Page {

		private final int pageSize;
		private final long totalRowCount;
		private final int pageIndex;
		private final List<Game> list;

		public Page(List<Game> data, long total, int page, int pageSize) {
			this.list = data;
			this.totalRowCount = total;
			this.pageIndex = page;
			this.pageSize = pageSize;
		}

		public long getTotalRowCount() {
			return totalRowCount;
		}

		public int getPageIndex() {
			return pageIndex;
		}

		public List<Game> getList() {
			return list;
		}

		public boolean hasPrev() {
			return pageIndex > 1;
		}

		public boolean hasNext() {
			return (totalRowCount / pageSize) >= pageIndex;
		}
	}

}
