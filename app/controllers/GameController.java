package controllers;

import com.fasterxml.jackson.databind.JsonNode;

import models.Board;
import models.Game;
import models.Move;
import models.Player;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import utils.Helpers;

public class GameController extends Controller {

	/**
	 * Display the paginated list of Games.
	 *
	 * @param page
	 *            Current page number (starts from 0)
	 * @param sortBy
	 *            Column to be sorted
	 * @param order
	 *            Sort order (either asc or desc)
	 * @param filter
	 *            Filter applied on game names
	 */
	@Transactional(readOnly = true)
	public static Result list(int page, String sortBy, String order,
			String filter) {
		return ok(Json.toJson(Game.page(page, 10, sortBy, order, filter)));
	}

	@Transactional(readOnly = true)
	public static Result show(Long id) {
		final Game game = Game.findById(id);
		if (game == null) {
			return notFound(Helpers.jsonMessage("message",
					String.format("Game %d does not exists.", id)));
		}
		return ok(Json.toJson(game));
	}

	@Transactional
	@BodyParser.Of(BodyParser.Json.class)
	public static Result create() {
		JsonNode json = request().body().asJson();
		String gameName = json.findPath("name").textValue();
		if (gameName == null) {
			return badRequest("Missing parameter [name]");
		}
		String player1Name = json.findPath("player1_name").textValue();
		if (player1Name == null) {
			return badRequest("Missing parameter [player1 name]");
		}
		String player2Name = json.findPath("player2_name").textValue();
		if (player2Name == null) {
			return badRequest("Missing parameter [player2 name]");
		}
		Game game = Game.createNewGame(gameName);
		Board board = Board.createNewBoard();
		Player player1 = Player.createNewPlayer(player1Name, 0);
		Player player2 = Player.createNewPlayer(player2Name, 1);
		game.board = board;
		board.game = game;

		player1.game = game;
		player2.game = game;
		game.players.add(0, player1);
		game.players.add(1, player2);
		game.currentPlayerId = player1.id;
		return ok(Json.toJson(game));
	}

	public static Result update(Long id) {
		return TODO;
	}

	@Transactional
	public static Result destroy(Long id) {
		final Game game = Game.findById(id);
		if (game == null) {
			return notFound(Helpers.jsonMessage("message",
					String.format("Game %d does not exists.", id)));
		}
		JPA.em().remove(game);
		return noContent();
	}

	@Transactional
	public static Result move(Long id, int pitNum) {
		Game game = Game.findById(id);
		if (game.status == Game.Status.IN_PROGRESS) {
			Board board = game.board;
			Player currentPlayer = Player.findById(game.currentPlayerId);
			String message = Helpers.performInitialPitNumChecks(currentPlayer,
					board, pitNum);
			if (message != null)
				return forbidden(Helpers.jsonMessage("message", message));
			Move move = Move.createNewMove(currentPlayer, pitNum,
					board.getStones(board.getPitNum(currentPlayer.playerNum,
							pitNum)));
			game.moves.add(move);
			move.game = game;
			boolean moveResult = board.makeAMove(currentPlayer.playerNum,
					pitNum);

			if (board.gameOver()) {
				board.emptyStonesIntoLubangMenggali();
				if (board.stonesInLubangMenggali(0) > board
						.stonesInLubangMenggali(1)) {
					game.currentPlayerId = game.players.get(0).id;
					game.status = Game.Status.WON;
					return ok(String.format("Player %s wins!",
							game.players.get(0).name));
				} else if (board.stonesInLubangMenggali(0) < board
						.stonesInLubangMenggali(1)) {
					game.currentPlayerId = game.players.get(1).id;
					game.status = Game.Status.WON;
					return ok(String.format("Player %s wins!",
							game.players.get(1).name));
				} else {
					game.status = Game.Status.TIED;
					return ok(String.format("This game is tied :-(."));
				}
			} else {
				if (!moveResult)
					if (currentPlayer.playerNum == 0)
						game.currentPlayerId = game.players.get(1).id;
					else
						game.currentPlayerId = game.players.get(0).id;
				return ok(Json.toJson(game));
			}
		}
		return ok("Game is already finished.");
	}

}
