package utils;

import models.Board;
import models.Player;
import play.libs.Json;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class Helpers {
	
	public static ObjectNode jsonMessage(String key, String message){
		ObjectNode result = Json.newObject();
		result.put(key, message);
		return result;
	}
	
	public static String performInitialPitNumChecks(Player currentPlayer, Board board, int pitNum){
		if (pitNum < 0 || pitNum > Board.playingPits)
			return "Pit number can only be between 1 and 6 (inclusive).";
		if (board.isLubangMenggali(board.getPitNum(currentPlayer.playerNum,
				pitNum)))
			return "This is your Lubang Menggali!";
		if (board.getStones(board
				.getPitNum(currentPlayer.playerNum, pitNum)) == 0)
			return "This pit is empty!";
		return null;
	}

}
