# Lubang Menggali
To run the project, type 'activator' in project's root directory and then 'run'. If you want to browse the database also then before 'run'ing it type 'h2-browser' and confirm that database URL is 'jdbc:h2:mem:play'.

## Game handlers
* *Creating a new game* - POST to http://127.0.0.1:9000/games with application/json payload of the format '{name: \<name_of_the_game\>, player1_name: \<name of player1\>, player2_name: \<name of player2\>}'.
* *List of games* - GET http://127.0.0.1:9000/games.
* *Details of a game* - GET http://127.0.0.1:9000/games/:id
* *Make a move* - GET http://127.0.0.1:9000/games/:id/move/:pit_number
* *Deleta a game* - DELETE http://127.0.0.1:9000/games/:id


